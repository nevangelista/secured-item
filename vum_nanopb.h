#ifndef VUM_NANOPB_H_INCLUDED
#define VUM_NANOPB_H_INCLUDED
#include "pb_decode.h"
#include "pb_encode.h"

#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

bool DecodeString(pb_istream_t* stream, const pb_field_t* field, void** arg);

bool EncodeString(pb_ostream_t* stream, const pb_field_t* field, void* const* arg);

bool ParseFromFile(pb_istream_t* stream, uint8_t* buf, size_t count);

bool SerializeToFile(pb_ostream_t* stream, const uint8_t* buf, size_t count);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
