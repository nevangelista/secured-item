#include "mbedtls/md.h"
#include "package.pb.h"
#include "security.pb.h"
#include "vum_nanopb.h"

#include <stdio.h>
#include <stdlib.h>

#define SHA256_HASH_SIZE 32
typedef PB_BYTES_ARRAY_T(SHA256_HASH_SIZE) SHA256Hash_t;

/**
 * @brief
 * @param hexme
 */
void HexifyByteArray(pb_bytes_array_t* hexme)
{
    for (int i = 0; i < hexme->size; i++)
    {
        printf("%x", (unsigned) hexme->bytes[i]);
    }
    printf("\n");
}

/*
 *  Structs for decoding EncapsulatedMessage (currently 1 per manifest)
 */
#define MAX_ENCAPSULATED_MESSAGES 4
struct EncapsulatedMessageData_t
{
    secured_item_EncapsulatedMessage* data[MAX_ENCAPSULATED_MESSAGES];
    int size;
};

struct EncapsulatedMessageData_t SsEncapsulatedMessages = {0};

/**
 * @brief
 * @param addMe
 */
void AddEncapsulatedMessage(secured_item_EncapsulatedMessage* addMe)
{
    if (SsEncapsulatedMessages.size == MAX_ENCAPSULATED_MESSAGES)
    {
        return;
    }

    SsEncapsulatedMessages.data[SsEncapsulatedMessages.size] = addMe;
    ++SsEncapsulatedMessages.size;
}

/*
 * Structs for decoding Signatures
 */
#define MAX_SIGNERS 4
struct SignatureChainData_t
{
    secured_item_Signature* data[MAX_SIGNERS];
    int size;
};

struct SignatureChainData_t SsSignatureChain = {0};

void AddSignatureChain(secured_item_Signature* addMe)
{
    if (SsSignatureChain.size == MAX_SIGNERS)
    {
        return;
    }
    SsSignatureChain.data[SsSignatureChain.size] = addMe;
    ++SsSignatureChain.size;
}

/*
 *  Structs for decoding DeviceTypes
 */
#define MAX_DEVICES 4
struct DestinationData_t
{
    secured_item_DeviceType data[MAX_DEVICES];
    int size;
};

struct DestinationData_t SsDestinations = {0};

void AddDestination(secured_item_DeviceType addMe)
{
    SsDestinations.data[SsDestinations.size] = addMe;
    ++SsDestinations.size;
}

/**
 * @brief
 * @param message
 */
SHA256Hash_t* ComputeMessageHash(pb_bytes_array_t* message)
{
    uint8_t messageHash[SHA256_HASH_SIZE];

    mbedtls_md_context_t ctx;
    mbedtls_md_type_t hashType = MBEDTLS_MD_SHA256;

    mbedtls_md_init(&ctx);
    mbedtls_md_setup(&ctx, mbedtls_md_info_from_type(hashType), 0);

    mbedtls_md_starts(&ctx);
    mbedtls_md_update(&ctx, (const unsigned char*) message->bytes, message->size);
    mbedtls_md_finish(&ctx, messageHash);

    mbedtls_md_free(&ctx);

    SHA256Hash_t* hash = malloc(sizeof(SHA256Hash_t));
    strncpy((char*) hash->bytes, (const char*) messageHash, SHA256_HASH_SIZE);
    hash->size = SHA256_HASH_SIZE;
    return hash;
}

/**
 * @brief
 * @param stream
 * @param field
 * @param arg
 * @return
 */
bool DecodeDestination(pb_istream_t* stream, const pb_field_t* field, void** arg)
{
    uint32_t value;
    if (!pb_decode_fixed32(stream, &value))
    {
        return false;
    }

    AddDestination(value);
    return true;
}

/**
 * @brief
 * @param stream
 * @param field
 * @param arg
 * @return
 */
bool DecodeEncapsulatedMessage(pb_istream_t* stream, const pb_field_t* field, void** arg)
{
    secured_item_EncapsulatedMessage* myEncapsulatedMessage =
        calloc(1, sizeof(secured_item_EncapsulatedMessage));

    typedef PB_BYTES_ARRAY_T(512) EncapsulatedMessage_serialized_message_t;
    EncapsulatedMessage_serialized_message_t* serializedMessageDecoder =
        malloc(sizeof(EncapsulatedMessage_serialized_message_t));
    myEncapsulatedMessage->serialized_message.funcs.decode = &DecodeString;
    myEncapsulatedMessage->serialized_message.arg          = serializedMessageDecoder;

    if (!pb_decode(stream, secured_item_EncapsulatedMessage_fields, myEncapsulatedMessage))
    {
        return false;
    }

    AddEncapsulatedMessage(myEncapsulatedMessage);

    return true;
}

/**
 * @brief
 * @param stream
 * @param field
 * @param arg
 * @return
 */
bool DecodeSignatureChain(pb_istream_t* stream, const pb_field_t* field, void** arg)
{
    secured_item_Signature* mySignature = calloc(1, sizeof(secured_item_Signature));

    typedef PB_BYTES_ARRAY_T(128) Signature_id_of_people_signing_t;
    Signature_id_of_people_signing_t* signersDecoder =
        malloc(sizeof(Signature_id_of_people_signing_t));
    mySignature->id_of_people_signing.funcs.decode = &DecodeString;
    mySignature->id_of_people_signing.arg          = signersDecoder;

    typedef PB_BYTES_ARRAY_T(SHA256_HASH_SIZE) Signature_encrypted_hash_t;
    Signature_encrypted_hash_t* encryptedHashDecoder = malloc(sizeof(Signature_encrypted_hash_t));
    mySignature->encrypted_hash.funcs.decode         = &DecodeString;
    mySignature->encrypted_hash.arg                  = encryptedHashDecoder;

    if (!pb_decode(stream, secured_item_Signature_fields, mySignature))
    {
        return false;
    }

    AddSignatureChain(mySignature);
    return true;
}

/**
 * @brief
 * @param argc
 * @param argv
 * @return
 */
int main(int argc, char** argv)
{
    if (argc == 1)
    /*-----------------------------------------------------------------------*\
    |   Usage note
    \*-----------------------------------------------------------------------*/
    {
        printf("usage: read_secured_item <path-to-secured-item");
        return -1;
    }

    /*-----------------------------------------------------------------------*\
    |   Read a SecuredItem (probably a .cpkg)
    \*-----------------------------------------------------------------------*/
    FILE* cpkgFile = fopen(*++argv, "rb");

    pb_istream_t istream = {&ParseFromFile, cpkgFile, SIZE_MAX};

    secured_item_SecuredItem mySecuredItem     = secured_item_SecuredItem_init_default;
    mySecuredItem.messages.funcs.decode        = &DecodeEncapsulatedMessage;
    mySecuredItem.signature_chain.funcs.decode = &DecodeSignatureChain;

    bool status = pb_decode(&istream, secured_item_SecuredItem_fields, &mySecuredItem);

    if (status)
    {
        for (int i = 0; i < SsEncapsulatedMessages.size; ++i)
        {
            printf("message %d            : \n", i);
            secured_item_EncapsulatedMessage* encapsulatedMessage = SsEncapsulatedMessages.data[i];
            pb_callback_t serializedMessage          = encapsulatedMessage->serialized_message;
            pb_bytes_array_t* serializedMessageBytes = (pb_bytes_array_t*) serializedMessage.arg;
            printf("serialized_message   : ");
            HexifyByteArray(serializedMessageBytes);

            SHA256Hash_t* messageHash = ComputeMessageHash(serializedMessageBytes);
            printf("hash                 : ");
            HexifyByteArray((pb_bytes_array_t*) messageHash);
            free(messageHash);
        }

        for (int i = 0; i < SsSignatureChain.size; ++i)
        {
            secured_item_Signature* mySignature = SsSignatureChain.data[i];
            if (mySignature->has_signature_time)
            {
                printf("signature_time       : %ld\n", (long int) mySignature->signature_time);
            }

            pb_bytes_array_t* signer = (pb_bytes_array_t*) mySignature->id_of_people_signing.arg;
            char whodat[64];
            strncpy(whodat, (const char*) signer->bytes, signer->size);
            printf("id_of_people_signing : %s\n", whodat);

            pb_bytes_array_t* hasher = (pb_bytes_array_t*) mySignature->encrypted_hash.arg;
            printf("encrypted_hash       : ");
            HexifyByteArray(hasher);
        }
    }

    fclose(cpkgFile);

    /*-----------------------------------------------------------------------*\
    |   Read the EncapsulatedMessage
    \*-----------------------------------------------------------------------*/
    for (int i = 0; i < SsEncapsulatedMessages.size; ++i)
    {
        secured_item_EncapsulatedMessage* encapsulatedMessage = SsEncapsulatedMessages.data[i];

        if (encapsulatedMessage->has_message_type &&
            encapsulatedMessage->message_type == secured_item_MessageTypes_MT_OTA_MANIFEST_PB)
        {
            pb_callback_t serializedMessage = encapsulatedMessage->serialized_message;
            pb_bytes_array_t* messageString = (pb_bytes_array_t*) serializedMessage.arg;

            pb_istream_t stream = pb_istream_from_buffer(messageString->bytes, messageString->size);
            // TBD
        }
    }
}
