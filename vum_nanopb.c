#include "vum_nanopb.h"

#include <stdlib.h>
#include <string.h>

/**
 * @fn      DecodeString
 * @brief   Decodes a variable-length string
 * @param   stream
 * @param   field
 * @param   arg
 * @return
 */
bool DecodeString(pb_istream_t* stream, const pb_field_t* field, void** arg)
{
    uint8_t* buffer = (uint8_t*) calloc(stream->bytes_left, sizeof(uint8_t));

    if (buffer == NULL)
    {
        return false;
    }

    size_t bytesToRead = stream->bytes_left;
    if (!pb_read(stream, buffer, stream->bytes_left))
    {
        return false;
    }

    pb_bytes_array_t* decoder = *arg;
    memcpy((void*) decoder->bytes, (const void*) buffer, bytesToRead);
    decoder->size = bytesToRead;
    free(buffer);
    return true;
}

/**
 * @fn      EncodeString
 * @brief   Encodes a variable-length string
 * @param   stream
 * @param   field
 * @return
 */
bool EncodeString(pb_ostream_t* stream, const pb_field_t* field, void* const* arg)
{
    char* str = NULL;

    if (arg == NULL)
    {
        return false;
    }
    else
    {
        str = (char*) *arg;
    }

    if (!pb_encode_tag_for_field(stream, field))
    {
        return false;
    }

    return pb_encode_string(stream, (uint8_t*) str, strlen(str));
}

/**
 * @fn      ParseFromFile
 * @brief   Parses a PB from a file
 * @param   stream
 * @param   buf
 * @param   count
 * @return
 */
bool ParseFromFile(pb_istream_t* stream, uint8_t* buf, size_t count)
{
    FILE* file = (FILE*) stream->state;

    if (buf == NULL)
    {
        while (count-- && fgetc(file) != EOF)
            ;
        return count == 0;
    }

    bool status = (fread(buf, 1, count, file) == count);

    if (feof(file))
        stream->bytes_left = 0;

    return status;
}

/**
 * @fn      SerializeToFile
 * @brief   Serializes PB to a file
 * @param   stream
 * @param   buf
 * @param   count
 * @return
 */
bool SerializeToFile(pb_ostream_t* stream, const uint8_t* buf, size_t count)
{
    FILE* file = (FILE*) stream->state;
    return fwrite(buf, 1, count, file) == count;
}
