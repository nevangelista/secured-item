# Include the nanopb provided Makefile rules
include ../../nanopb-dist/extra/nanopb.mk

# Compiler flags to enable all warnings & debug info
MBED_DIR = ../../mbedtls

CFLAGS = -Wall -g -O0 -DPB_FIELD_16BIT
CFLAGS += -I$(NANOPB_DIR) -I$(MBED_DIR)/include

# C source code files that are required
CSRC  = read_secured_item.c  		# The main program
CSRC += security.pb.c				# The compiled protocol definition
CSRC += package.pb.c				# The compiled protocol definition
CSRC += vum_nanopb.c				# The compiled protocol definition

CSRC += $(NANOPB_DIR)/pb_encode.c 	# The nanopb encoder
CSRC += $(NANOPB_DIR)/pb_decode.c 	# The nanopb decoder
CSRC += $(NANOPB_DIR)/pb_common.c  	# The nanopb common parts

MLIB = $(MBED_DIR)/library

# Build rule for the main program
read_secured_item: $(CSRC)
	$(CC) $(CFLAGS) -o read_secured_item $(CSRC) -lmbedcrypto

# Build rule for the protocol
security.pb.c: security.proto
	$(PROTOC) $(PROTOC_OPTS) --nanopb_out=. security.proto

package.pb.c: package.proto
	$(PROTOC) $(PROTOC_OPTS) --nanopb_out=. package.proto

clean:
	rm *.o *.pb.c *.pb.h read_secured_item

.PHONY: all clean


